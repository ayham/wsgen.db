# wsgen.db

This repository contains the official database for the wsgen project. Here, you
would find the scripts for generating a database of parsed questions,
markschemes and examiner reports. These scripts are intended for use by
developers of projects that need parsed past-papers from Cambridge CAIE
examinations.

This project is under development.

## Installation

To start developing, run the following commands:

```bash
git clone https://codeberg.org/ayham/wsgen.db.git
cd wsgen.db
pip3 install -r requirements.txt
pip3 install -r requirements-dev.txt
pre-commit install
```

## Usage
### Scraping

For systemic testing run: `pytest`, otherwise you can use the scraper scripts
directly:

```bash
./wsgendb/scraper/qp.py [pdf location her] [start page here] [render] \
						[render cropped] [full debug, i.e. screaming fury]
./wsgendb/scraper/qp.py 9700_s21_qp_21.pdf 2 1 0 0
```

Currently, only the question paper scraper work.


### Downloading

In order to download the entirety of subject's past papers run the following
command:

```bash
./wsgendb/downloader.py
```

This downloads Maths A level's past papers. You have to open and edit the
`__main__` code in order to set-up meta-info for other subjects. (TODO: build a system for
submitting and receiving meta-info of past-papers)


# Help Wanted!

TODO: start doing open-source stuff.

You can contribute by:
	- [teacher contrib]
	- [dev contrib]
	- [funding contrib]

# License

This repository is licensed under GPLv-2. Changes are welcome.

These are stuff what I seem fit for this repository (again, suggestions are
needed):
- Maintain copyright and author information in files.
- Contribute your changes to the main project.


Initial author: [ayham](https://ayham.xyz)
