#!/usr/bin/python3

import sys
import pytest

from wsgendb.scraper.qp import scrape_qp

# TODO: assert boundingBox


@pytest.mark.datafiles("tests/data/9700_s21_qp_21.pdf")
def test_9700_s21_qp_21():
    result = scrape_qp(
        "tests/data/9700_s21_qp_21.pdf",
        start_page=2,
        render=False,
        render_cropped=False,
        screaming_fury="1",
        pad=[5, 5],
    )

    assert len(result) == 2
    assert len(result[0]) == 28
    assert len(result[1]) == 15


@pytest.mark.datafiles("tests/data/9700_m21_qp_22.pdf")
def test_9700_m21_qp_22():
    result = scrape_qp(
        "tests/data/9700_m21_qp_22.pdf",
        start_page=2,
        render=False,
        render_cropped=False,
        screaming_fury="1",
        pad=[5, 5],
    )

    assert len(result) == 2
    assert len(result[0]) == 28
    assert len(result[1]) == 15
