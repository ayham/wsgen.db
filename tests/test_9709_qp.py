#!/usr/bin/python3

import sys
import pytest

from wsgendb.scraper.qp import scrape_qp

# TODO: assert boundingBox


@pytest.mark.datafiles("tests/data/9709_s20_qp_12.pdf")
def test_9709_s20_qp_12():
    result = scrape_qp(
        "tests/data/9709_s20_qp_12.pdf",
        start_page=2,
        render=False,
        render_cropped=False,
        screaming_fury="1",
        pad=[5, 5],
    )

    assert len(result) == 2
    assert len(result[0]) == 26
    assert len(result[1]) == 15


@pytest.mark.datafiles("tests/data/9709_s20_qp_22.pdf")
def test_9709_s20_qp_22():
    result = scrape_qp(
        "tests/data/9709_s20_qp_22.pdf",
        start_page=3,
        render=False,
        render_cropped=False,
        screaming_fury="1",
        pad=[5, 5],
    )

    assert len(result) == 2
    assert len(result[0]) == 15
    assert len(result[1]) == 14


@pytest.mark.datafiles("tests/data/9709_s20_qp_32.pdf")
def test_9709_s20_qp_32():
    result = scrape_qp(
        "tests/data/9709_s20_qp_32.pdf",
        start_page=2,
        render=False,
        render_cropped=False,
        screaming_fury="1",
        pad=[5, 5],
    )

    assert len(result) == 2
    assert len(result[0]) == 18
    assert len(result[1]) == 19


@pytest.mark.datafiles("tests/data/9709_s20_qp_42.pdf")
def test_9709_s20_qp_42():
    result = scrape_qp(
        "tests/data/9709_s20_qp_42.pdf",
        start_page=2,
        render=False,
        render_cropped=False,
        screaming_fury="1",
        pad=[5, 5],
    )

    assert len(result) == 2
    assert len(result[0]) == 14
    assert len(result[1]) == 11


@pytest.mark.datafiles("tests/data/9709_s20_qp_52.pdf")
def test_9709_s20_qp_52():
    result = scrape_qp(
        "tests/data/9709_s20_qp_52.pdf",
        start_page=2,
        render=False,
        render_cropped=False,
        screaming_fury="1",
        pad=[5, 5],
    )

    assert len(result) == 2
    assert len(result[0]) == 19
    assert len(result[1]) == 15


@pytest.mark.datafiles("tests/data/9709_s20_qp_62.pdf")
def test_9709_s20_qp_62():
    result = scrape_qp(
        "tests/data/9709_s20_qp_62.pdf",
        start_page=2,
        render=False,
        render_cropped=False,
        screaming_fury="1",
        pad=[5, 5],
    )

    assert len(result) == 2
    assert len(result[0]) == 19
    assert len(result[1]) == 11
