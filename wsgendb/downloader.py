#!/usr/bin/python3

import os
import pathlib
import requests
import time
import hashlib
import json
import filetype
import threading
import signal

from tqdm import tqdm
from concurrent.futures import ThreadPoolExecutor, as_completed

pool = ThreadPoolExecutor(max_workers=35)
futures = []
tqdm.get_lock()

bytes_downloaded = 0
bytes_lock = threading.Lock()

md5_table = {}
md5_table_file = ""

# TODO: add single paper, ex practical papers, which has multiple variants for multiple years
def download_subject(
    url,
    subject,
    year_start,
    year_end,
    raw_dir,
    sessions,
    variants,
    papers,
    no_variant_years=None,
    merged_ms_years=None,
    merged_ms_papers=None,
    second_variant_session=None,
    half_variant_years=None,
    half_variant_sessions_skip=None,
    half_variants=None,
    metaDir="meta/",
):
    tic = time.perf_counter()

    global md5_table_file
    md5_table_file = metaDir + "raw." + subject + ".md5.json"

    signal.signal(signal.SIGINT, handler)

    # try to resume
    try:
        with open(md5_table_file, "r") as file:
            global md5_table
            md5_table = json.load(file)
            assert type(md5_table == dict)
            print("resuming download...")
    except:
        print("did not find valid resume md5 file, restarting download...")
        md5_table = {}

    # assert givens
    assert len(year_start) == 4
    assert len(year_end) == 4
    assert url[-1] == "/"

    start = int(year_start[2:4])
    end = int(year_end[2:4])

    assert end >= start

    assert type(sessions) is list
    assert type(variants) is list
    assert type(papers) is list

    assert raw_dir[-1] == "/"

    if type(merged_ms_years) == list:
        assert type(merged_ms_papers) == list
        assert len(merged_ms_papers) > 0

    # build directory
    assert_path(raw_dir)
    assert_path(raw_dir + subject)
    assert_path(metaDir)

    for year in range(start, end + 1, 1):
        year = str(year).rjust(2, "0")
        url_prefix = url + "20" + year + "/" + subject + "_"
        dir_prefix = raw_dir + subject + "/" + "20" + year + "/"

        # assert year directory
        assert_path(dir_prefix)

        # assert sessions directory, may/june
        for session in sessions:
            path = dir_prefix + subject + "_" + session + year

            # download examiner report for this session
            erURL = url_prefix + session + year + "_er.pdf"
            erPath = path + "_er.pdf"

            download_url_threaded(
                erURL, erPath, subject + "_" + session + year + "_er.pdf"
            )

            if type(no_variant_years) == list and ("20" + year) in no_variant_years:
                is_variant_session = False
            else:
                is_variant_session = True

            if type(half_variant_years) == list and ("20" + year) in half_variant_years:
                is_half_year = True
                if session in half_variant_sessions_skip:
                    is_variant_session = False
            else:
                is_half_year = False

            if is_variant_session:
                for variant in variants:  # variant 1, 2
                    if (
                        type(second_variant_session) == list
                        and session in second_variant_session
                        and variant != "2"
                    ):
                        continue
                    if is_half_year and not variant in half_variants:
                        continue

                    for paper in papers:  # paper 1, 3
                        # download question paper
                        qURL = (
                            url_prefix
                            + session
                            + year
                            + "_qp_"
                            + paper
                            + variant
                            + ".pdf"
                        )
                        qPath = (
                            dir_prefix
                            + subject
                            + "_"
                            + session
                            + year
                            + "_qp_"
                            + paper
                            + variant
                            + ".pdf"
                        )
                        label = (
                            subject
                            + "_"
                            + session
                            + year
                            + "_ms_"
                            + paper
                            + variant
                            + ".pdf"
                        )

                        download_url_threaded(qURL, qPath, label)

                        # download unmerged ms for variant
                        m_url = (
                            url_prefix
                            + session
                            + year
                            + "_ms_"
                            + paper
                            + variant
                            + ".pdf"
                        )
                        m_path = (
                            dir_prefix
                            + subject
                            + "_"
                            + session
                            + year
                            + "_ms_"
                            + paper
                            + variant
                            + ".pdf"
                        )
                        label = (
                            subject
                            + "_"
                            + session
                            + year
                            + "_ms_"
                            + paper
                            + variant
                            + ".pdf"
                        )
                        download_url_threaded(m_url, m_path, label)
            else:
                for paper in papers:  # paper 1, 3
                    if (
                        type(merged_ms_years) == list
                        and ("20" + year) in merged_ms_years
                    ):
                        is_merged_ms_year = True
                    else:
                        is_merged_ms_year = False

                    # download question paper
                    qURL = url_prefix + session + year + "_qp_" + paper + ".pdf"
                    qPath = (
                        dir_prefix
                        + subject
                        + "_"
                        + session
                        + year
                        + "_qp_"
                        + paper
                        + ".pdf"
                    )
                    label = subject + "_" + session + year + "_ms_" + paper + ".pdf"

                    download_url_threaded(qURL, qPath, label)

                    # download ms
                    if not is_merged_ms_year:
                        m_url = url_prefix + session + year + "_ms_" + paper + ".pdf"
                        m_path = (
                            dir_prefix
                            + subject
                            + "_"
                            + session
                            + year
                            + "_ms_"
                            + paper
                            + ".pdf"
                        )
                        label = subject + "_" + session + year + "_ms_" + paper + ".pdf"
                    else:
                        m_url = (
                            url_prefix + session + year + "_ms_" + merged_ms_papers[0]
                        )
                        m_path = (
                            dir_prefix
                            + subject
                            + "_"
                            + session
                            + year
                            + "_ms_"
                            + merged_ms_papers[0]
                        )
                        label = (
                            subject
                            + "_"
                            + session
                            + year
                            + "_ms_"
                            + merged_ms_papers[0]
                        )
                        for paper in merged_ms_papers[1:]:
                            m_url = m_url + "+" + paper
                            m_path = m_path + "+" + paper
                            label = label + "+" + paper
                        m_url = m_url + ".pdf"
                        m_path = m_path + ".pdf"
                        label = label + ".pdf"

                    download_url_threaded(m_url, m_path, label)

    print("waiting for everyone to execute...")

    global futures
    finalProgressBar = tqdm(total=len(futures))
    for future in as_completed(futures):
        del future
        finalProgressBar.update(1)

    print("dumping md5 meta table...")
    with open(md5_table_file, "w") as convert_file:
        convert_file.write(json.dumps(md5_table, indent=4, sort_keys=True))

    toc = time.perf_counter()

    size = "{:.1f}".format((get_directory_size(raw_dir + subject) / 1024) / 1024)
    dlSize = "{:.1f}".format((bytes_downloaded / 1024) / 1024)
    try:
        efficiency = "{:.1f}%".format(
            (get_directory_size(raw_dir + subject) / bytes_downloaded) * 100
        )
    except:
        efficiency = "non"

    print(f"total time taken: {toc - tic:0.4f} seconds")
    print(f"total megabytes downloaded: { dlSize } mb")
    print(f"size on disk: { size } mb")
    print(f"efficiency of this download: { efficiency }")


def download_url_threaded(url, path, label):
    # check if file is already downloaded
    if path in md5_table.keys():
        print(f"already downloaded file { label }, skipping...")
        return

    futures.append(pool.submit(download_url, url, path, label))


def download_url(url, path, label):
    # Streaming, so we can iterate over the response.
    response = requests.get(url, stream=True)
    total_size_in_bytes = int(response.headers.get("content-length", 0))
    block_size = 1024  # 1 Kibibyte
    progress_bar = tqdm(
        total=total_size_in_bytes, unit="B", unit_scale=True, leave=False
    )
    hash_md5 = hashlib.md5()
    with open(path, "wb") as file:
        for data in response.iter_content(block_size):
            progress_bar.set_postfix(file=label, refresh=False)
            progress_bar.update(len(data))
            file.write(data)
            hash_md5.update(data)
        if is_pdf(path):
            md5_table[path] = hash_md5.hexdigest()
        else:
            os.remove(path)

    progress_bar.close()
    if total_size_in_bytes != 0 and progress_bar.n != total_size_in_bytes:
        print("ERROR, something went wrong")
    else:
        bytes_lock.acquire()
        global bytes_downloaded
        bytes_downloaded = bytes_downloaded + total_size_in_bytes
        bytes_lock.release()


def assert_path(path):
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)


def is_pdf(path):
    try:
        return filetype.guess(path).mime == "application/pdf"
    except:
        return False


def get_directory_size(directory):
    total = 0
    try:
        for entry in os.scandir(directory):
            if entry.is_file():
                # if it's a file, use stat() function
                total += entry.stat().st_size
            elif entry.is_dir():
                # if it's a directory, recursively call this function
                try:
                    total += get_directory_size(entry.path)
                except FileNotFoundError:
                    pass
    except NotADirectoryError:
        # if `directory` isn't a directory, get the file size then
        return os.path.getsize(directory)
    except PermissionError:
        # if for whatever reason we can't open the folder, return 0
        return 0
    return total


def handler(signum, frame):
    del signum, frame
    print("dumping md5 meta table...")
    global md5_table_file
    with open(md5_table_file, "w") as convert_file:
        convert_file.write(json.dumps(md5_table, indent=4, sort_keys=True))
    os._exit(1)


def generate_meta(subject):
    pass


if __name__ == "__main__":
    url = "https://papers.gceguide.com/A%20Levels/Mathematics%20(9709)/"
    subject = "9709"
    year_start = "2001"
    year_end = "2021"
    raw_dir = "raw/"
    sessions = ["m", "s", "w"]
    variants = ["1", "2", "3"]
    papers = ["1", "2", "3", "4", "5", "6", "7"]
    no_variant_years = (
        ["2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008"],
    )
    merged_ms_years = ["2003"]
    merged_ms_papers = ["1", "2", "3", "4", "5", "6", "7"]
    second_variant_session = ["m"]
    half_variant_years = ["2009"]
    half_variant_sessions_skip = ["s"]
    half_variants = ["1", "2"]

    download_subject(
        url,
        subject,
        year_start,
        year_end,
        raw_dir,
        sessions,
        variants,
        papers,
        no_variant_years,
        merged_ms_years,
        merged_ms_papers,
        second_variant_session,
        half_variant_years,
        half_variant_sessions_skip,
        half_variants,
    )
