#!/usr/bin/python3

import sys
import cv2
import numpy as np
import pdf2image

from collections import defaultdict
from typing import BinaryIO, Container, Optional, cast

from pdfminer.converter import PDFPageAggregator
from pdfminer.layout import LAParams
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.utils import open_filename, FileOrName
from pdfminer.layout import LTTextContainer, LTChar, LTTextLine

# TODO (DELAYED): search for higher Y values in same line of found start marker in order to
# dynamically calculate padding values of quesitons
# TODO (DELAYED): maybe fix the parts of the questions between the dotted lines
# and the next question marker
# TODO: add progress bar feedback


def scrape_qp(
    file,
    pad=[0, 0],
    start_page=0,
    render=False,
    render_cropped=False,
    screaming_fury=False,
):
    start_page -= 1
    dpi = 72
    pages_img = convert_pdf_to_images(file, dpi)
    pages_pdf = extract_pages(file)

    expected_question = 1
    page_no = 0
    markers = []
    dotted_lines = []

    first_elements = []
    last_elements = []
    last_elements_horiz = []

    # find footer UCLES marker
    footer_marker = None
    for page in pages_pdf:
        for element in page:
            if isinstance(element, LTTextContainer):
                for text_line in element:
                    if "UCLES" in text_line.get_text():
                        footer_marker = text_line

    for page in pages_pdf[start_page:]:
        # find the first character in the page
        first_element = None
        for element in page:
            # skip over page numbers, which are centered
            if element.x0 < page.width * 0.15 and element.x0 > 0:
                first_element = element
                first_elements.append(first_element)
                break

        # find the last character in the page
        last_element = None
        for element in page:
            # skip over page footer
            if element.y0 > footer_marker.y0 + page.height * 0.01:
                if last_element is None:
                    last_element = element
                elif last_element.y0 > element.y0:
                    last_element = element

        last_elements.append(last_element)

        # find the last horizontal character in the page
        last_element_horiz = None
        for element in page:
            if element.x1 < page.width:
                if last_element_horiz is None:
                    last_element_horiz = element
                elif last_element_horiz.x1 <= element.x1:
                    last_element_horiz = element

        last_elements_horiz.append(last_element_horiz)

        # find all dotted lines blocks on this page
        line_box_start = None
        line_box_end = None
        temp_line_boxes = []
        previous_line = None
        for element in page:
            if isinstance(element, LTTextContainer):
                for text_line in element:
                    if not isinstance(text_line, LTTextLine):
                        continue

                    is_dotted = is_dotted_line(text_line)
                    if is_dotted:
                        previous_line = text_line

                    if is_dotted and line_box_start is None:
                        line_box_start = previous_line
                    elif (
                        (not is_dotted)
                        and (not line_box_start is None)
                        and not previous_line is text_line
                    ):
                        line_box_end = previous_line

                    if line_box_start and line_box_end:
                        temp_line_boxes.append(
                            [line_box_start, line_box_end, text_line]
                        )
                        line_box_start, line_box_end = None, None
                        previous_line = None
        dotted_lines.append(temp_line_boxes)

        # find all questions on this page
        temp_markers = []
        for element in page:
            if isinstance(element, LTTextContainer):
                for text_line in element:
                    q_num = ""
                    number_chars = []
                    searching_for_sub_question = False
                    sub_question_chars = []
                    end_marker_searching = False
                    end_marker_text = []

                    if not isinstance(text_line, LTTextLine):
                        continue

                    for character in text_line:
                        # check for new questions
                        if (
                            isinstance(character, LTChar)
                            and character.get_text().isdigit()
                        ):
                            if "Bold" in character.fontname:
                                q_num += str(int(character.get_text()))
                                number_chars.append(character)
                        # check for new subquestions
                        elif (
                            isinstance(character, LTChar)
                            and "Bold" in character.fontname
                        ):
                            if "(" in character.get_text():
                                searching_for_sub_question = True
                                sub_question_chars.append(character)
                            elif (
                                ")" in character.get_text()
                                and searching_for_sub_question
                            ):
                                searching_for_sub_question = False
                                sub_question_chars.append(character)
                                temp_markers.append(
                                    {
                                        "page": page_no,
                                        "marker": sub_question_chars,
                                        "start": True,
                                        "isSub": True,
                                        "level": "".join(
                                            a.get_text() for a in sub_question_chars
                                        ),
                                    }
                                )
                                sub_question_chars = []
                            elif searching_for_sub_question:
                                sub_question_chars.append(character)
                        # check for question end markers
                        elif isinstance(character, LTChar):
                            # could be the marks indicator
                            if character.get_text() == "[":
                                end_marker_searching = True
                            if character.get_text() == "]" and end_marker_searching:
                                end_marker_text.append(character)
                                if (
                                    end_marker_text[1].get_text() == "T"
                                    and end_marker_text[2].get_text() == "o"
                                ):
                                    end_marker_text = []
                                    end_marker_searching = False
                                else:
                                    temp_markers.append(
                                        {
                                            "page": page_no,
                                            "marker": [character],
                                            "start": False,
                                            "taken": False,
                                            "isSub": False,
                                        }
                                    )
                                    end_marker_text = []
                                    end_marker_searching = False
                        if end_marker_searching:
                            end_marker_text.append(character)
                    if q_num != "":
                        # check if this is not a page number
                        if number_chars[0].x0 < page.width * 0.10:
                            # number_chars =
                            assert expected_question == int(q_num)
                            expected_question += 1
                            corrected_marker = []
                            for item in element:
                                for character in item:
                                    if character.get_text().isspace():
                                        break
                                    else:
                                        corrected_marker.append(character)
                            temp_markers.append(
                                {
                                    "page": page_no,
                                    "marker": corrected_marker,
                                    "start": True,
                                    "isSub": False,
                                }
                            )
        markers.append(temp_markers)
        page_no += 1

    # build bounding boxes
    page_no = 0
    bounding_boxes = defaultdict(lambda: [])
    questions_num_on_pages = defaultdict(lambda: 0)
    for (page, paged_marker, first_element, last_element, last_element_horiz) in zip(
        pages_pdf[start_page:],
        markers,
        first_elements,
        last_elements,
        last_elements_horiz,
    ):
        is_started = False
        p_start = None
        p_end = None
        last_sub = None
        i, j = 0, 0
        while i < len(paged_marker):
            marker = paged_marker[i]
            is_skippable = False

            if marker["start"] and not is_started:
                is_started = True
                use = True
                if not marker["isSub"]:
                    is_skippable = True
                else:
                    if last_sub is None or not equal_sub_levels(
                        last_sub["level"][1:-1], marker["level"][1:-1]
                    ):
                        # allow skipping if we are dealing with a different level
                        # subquestion relative to the previous
                        is_skippable = True
                    else:
                        level = marker["level"][1:-1]

                        # do not accept subquestion if previous subquestion is not
                        # consecutive
                        if not (last_sub is None) and not (
                            after_sub(level, last_sub["level"][1:-1])
                        ):
                            use = False
                    last_sub = marker

                if use:
                    # use the first element in the page if the element is higher than
                    # the start marker and no boxes are found yet.
                    # inverted sign because pdf coord space starts from bottom-(west?)
                    if (
                        i == 0
                        and marker["start"]
                        and marker["marker"][0].y0 < first_element.y1
                    ):
                        p_start = convert_coords(
                            (first_element.x0, first_element.y1),
                            dpi,
                            72,
                            page.height,
                        )
                    else:
                        p_start = convert_coords(
                            (marker["marker"][0].x0, marker["marker"][0].y1),
                            dpi,
                            72,
                            page.height,
                        )
                    questions_num_on_pages[page_no] += 1

                # search for end markers
                j = i + 1
                while j < len(paged_marker):
                    marker = paged_marker[j]

                    # make sure to allow skippability when searching for an end
                    # marker and a different level subquestion is found
                    if (
                        marker["isSub"]
                        and not last_sub is None
                        and not equal_sub_levels(
                            marker["level"][1:-1], last_sub["level"][1:-1]
                        )
                    ):
                        is_skippable = True

                    # maintain a correct last_sub variable
                    if is_skippable and marker["isSub"]:
                        last_sub = marker

                    if not marker["start"] and not marker["taken"] and is_started:
                        is_started = False
                        marker["taken"] = True
                        p_end = convert_coords(
                            (marker["marker"][0].x1, marker["marker"][0].y0),
                            dpi,
                            72,
                            page.height,
                        )
                        break
                    j += 1

                if not p_start is None and not p_end is None:
                    bounding_boxes[page_no].append((p_start, p_end))

                    if is_skippable:
                        i = j
                        is_skippable = False
                    p_start = None
                    p_end = None

                # page ended without an end marker
                # i.e. question spans over two pages
                if (not p_start is None) and p_end is None:
                    # add bounding box for current page
                    p_end = convert_coords(
                        (last_element_horiz.x1, last_element.y0),
                        dpi,
                        72,
                        page.height,
                    )
                    bounding_boxes[page_no].append((p_start, p_end))
                    if is_skippable:
                        i = j
                        is_skippable = False
                    p_start = None
                    p_end = None
                else:
                    pass
            i += 1

        if (
            len(bounding_boxes[page_no]) == 0
            and len(paged_marker)
            and not paged_marker[0]["start"]
        ):
            # this is a page where there is no start marker, with a trailing
            # question from previous page
            p_start = convert_coords(
                (first_element.x0, first_element.y1),
                dpi,
                72,
                page.height,
            )
            p_end = convert_coords(
                (paged_marker[0]["marker"][0].x1, paged_marker[0]["marker"][0].y0),
                dpi,
                72,
                page.height,
            )
            questions_num_on_pages[page_no] += 1
            bounding_boxes[page_no].append((p_start, p_end))

        # Check if there is a crossing text on the bounding box of the question
        # and add the difference to the bounding box. This is a bit of an
        # expensive process
        # for page_no, page in enumerate(pages_pdf):
        #    for element in page:
        #        for box_no, box in enumerate(bounding_boxes[page_no]):
        #            if box[0][0] > element.x0 and box[1][0] < element.x1:
        #                if element.y0 < box[0][1] and element.y1 > box[0][1]:
        #                    bounding_boxes[page_no][box_no].y0 = \
        #                    bounding_boxes[page_no][box_no].y0 - \
        #                            abs(element.y0 - element.y1) / 2

        #                if element.y0 < box[1][1] and element.y1 > box[1][1]:
        #                    bounding_boxes[page_no][box_no].y1 + \
        #                            abs(element.y0 - element.y1) / 2

        # error handling per page
        if screaming_fury or questions_num_on_pages[page_no] != len(
            bounding_boxes[page_no]
        ):
            # mistake in parsing order of pdf
            print(
                f"=================================SCREAM Page: { page_no + 1 + start_page}"
            )
            print(f"Bounding boxes: {len(bounding_boxes[page_no])}")
            print(f"Questions on page: {questions_num_on_pages[page_no]}")
            print(f"First element: {first_elements[page_no]}")
            print(f"Paged markers: {paged_marker}")
            print(f"Paged bounding boxes: {bounding_boxes[page_no]}")

            for marker in paged_marker:
                for elem in marker["marker"]:
                    print(elem.get_text(), end="")
                print("")
            print(f"============================END Page: { page_no + 1 + start_page}")
            if not screaming_fury:
                return []

        page_no += 1

    # render
    page_no = 0
    images = []
    for (
        page,
        pdfPage,
        paged_marker,
        paged_dotted_lines,
        first_element,
        last_element,
        last_element_horiz,
    ) in zip(
        pages_img[start_page:],
        pages_pdf[start_page:],
        markers,
        dotted_lines,
        first_elements,
        last_elements,
        last_elements_horiz,
    ):
        original = page.copy()
        for box in bounding_boxes[page_no]:
            p1 = [int(x) for x in box[0]]
            p2 = [int(x) for x in box[1]]
            p1[0] -= pad[0]
            p1[1] -= pad[1]
            p2[0] += pad[0]
            p2[1] += pad[1]
            images.append((original.copy())[p1[1] : p2[1], p1[0] : p2[0]])
            page = cv2.rectangle(page, p1, p2, (0, 0, 255), 1)

            if render_cropped:
                cv2.imshow("Scraper", images[-1])
                cv2.waitKey(0)

        for marker in paged_marker:
            if marker["page"] == page_no:
                p1 = convert_coords(
                    (marker["marker"][0].x0, marker["marker"][0].y0),
                    dpi,
                    72,
                    pdfPage.height,
                )
                p2 = convert_coords(
                    (marker["marker"][0].x1, marker["marker"][0].y1),
                    dpi,
                    72,
                    pdfPage.height,
                )
                page = cv2.rectangle(page, p1, p2, (0, 0, 255), 1)

        # render dotted lines boxes
        for box in paged_dotted_lines:
            p1 = convert_coords(
                (box[0].x0, box[0].y1),
                dpi,
                72,
                pdfPage.height,
            )
            p2 = convert_coords(
                (box[1].x1, box[1].y0),
                dpi,
                72,
                pdfPage.height,
            )
            page = cv2.rectangle(page, p1, p2, (255, 0, 255), 1)
            # post dotted-lines
            p1 = convert_coords(
                (box[2].x0, box[2].y0),
                dpi,
                72,
                pdfPage.height,
            )
            p2 = convert_coords(
                (box[2].x1, box[2].y1),
                dpi,
                72,
                pdfPage.height,
            )
            page = cv2.rectangle(page, p1, p2, (255, 50, 255), 1)

        # render first element
        p1 = convert_coords(
            (first_element.x0, first_element.y0), dpi, 72, pdfPage.height
        )
        p2 = convert_coords(
            (first_element.x1, first_element.y1), dpi, 72, pdfPage.height
        )
        page = cv2.rectangle(page, p1, p2, (0, 255, 0), 1)

        # render last element
        p1 = convert_coords((last_element.x0, last_element.y0), dpi, 72, pdfPage.height)
        p2 = convert_coords((last_element.x1, last_element.y1), dpi, 72, pdfPage.height)
        page = cv2.rectangle(page, p1, p2, (255, 0, 0), 1)

        # render last horiz element
        p1 = convert_coords(
            (last_element_horiz.x0, last_element_horiz.y0),
            dpi,
            72,
            pdfPage.height,
        )
        p2 = convert_coords(
            (last_element_horiz.x1, last_element_horiz.y1),
            dpi,
            72,
            pdfPage.height,
        )
        page = cv2.rectangle(page, p1, p2, (0, 140, 255), 1)

        if render:
            cv2.imshow("Scraper", page)
            cv2.waitKey(0)

        page_no += 1

    cv2.destroyAllWindows()
    return [images, markers]


def after_sub(left: str, right: str):
    if numerical_sub_index(left) > numerical_sub_index(right):
        return True
    return False


def equal_sub_levels(left: str, right: str):
    if is_roman(left) and not is_roman(right):
        return False
    elif is_roman(right) and not is_roman(left):
        return False
    return True


def numerical_sub_index(sub: str):
    if is_roman(sub):
        return int(roman_to_decimal(sub))
    elif sub.isdigit():
        return int(sub)
    elif sub.isalpha():
        return int(ord(sub.lower()) - 96)
    return 0


def roman_to_decimal(inp):
    inp = inp.upper()
    roman = {"I": 1, "V": 5, "X": 10, "L": 50}
    inpNum = [roman[x] for x in inp]
    return sum(
        [
            -x if i < len(inpNum) - 1 and x < inpNum[i + 1] else x
            for i, x in enumerate(inpNum)
        ]
    )


def is_roman(string):
    import re

    for item in string:
        if not bool(
            re.search(r"^M{0,3}(X|XL|L?X{0,3})(IX|IV|V?I{0,3})$", item.upper())
        ):
            return False
    return True


def convert_pdf_to_images(document, dpi=72):
    images = []
    images.extend(
        list(
            map(
                lambda image: cv2.cvtColor(np.array(image), code=cv2.COLOR_RGB2BGR),
                pdf2image.convert_from_path(document, dpi=dpi),
            )
        )
    )
    return images


def convert_coords(point, target, current, height):
    ratio = target / current
    return (int(point[0] * ratio), int(height * ratio) - int(point[1] * ratio))


def is_dotted_line(text_line):
    dot_count = 0
    for character in text_line:
        if (
            not "(cid" in character.get_text()
            and character.get_text() != "."
            and character.get_text() != "\n"
        ):
            return False
        else:
            dot_count += 1

    if dot_count <= 5:
        return False

    return True


def extract_pages(
    pdf_file: FileOrName,
    password: str = "",
    page_numbers: Optional[Container[int]] = None,
    maxpages: int = 0,
    caching: bool = True,
    laparams: Optional[LAParams] = None,
):
    laparams = LAParams()
    results = []

    with open_filename(pdf_file, "rb") as fp:
        fp = cast(BinaryIO, fp)  # we opened in binary mode
        resource_manager = PDFResourceManager(caching=caching)
        device = PDFPageAggregator(resource_manager, laparams=laparams)
        interpreter = PDFPageInterpreter(resource_manager, device)
        for page in PDFPage.get_pages(
            fp, page_numbers, maxpages=maxpages, password=password, caching=caching
        ):
            interpreter.process_page(page)
            layout = device.get_result()
            results.append(layout)

    return results


if __name__ == "__main__":
    result = scrape_qp(
        sys.argv[1],
        start_page=int(sys.argv[2]),
        render=(sys.argv[3] == "1"),
        render_cropped=(sys.argv[4] == "1"),
        screaming_fury=(sys.argv[5] == "1"),
        pad=[5, 5],
    )

    print(len(result[0]))
    print(len(result[1]))

    del result
