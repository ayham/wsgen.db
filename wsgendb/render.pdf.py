#!/usr/bin/python3
# renders a pdf with bounded boxes using opencv and pdfminer.six

import sys
import cv2
import numpy as np
import pdf2image
from pdfminer.high_level import extract_pages


def render(file):
    pages = convertPDFToImages(file, 92)
    boxesPages = parsePDF(file)
    assert len(pages) == len(boxesPages)
    for page, boxes in zip(pages, boxesPages):
        pageHeight = page.shape[0]
        for box in boxes:
            p1 = convertDPI((box[0], box[1]), 92, 72, pageHeight)
            p2 = convertDPI((box[2], box[3]), 92, 72, pageHeight)
            color = (255, 0, 0)
            thickness = 2
            page = cv2.rectangle(page, p1, p2, color, thickness)

    for page in pages:
        cv2.imshow("", page)
        cv2.waitKey(0)


def parsePDF(file):
    boundingBoxes = []  # by pages
    for page_layout in extract_pages(file):
        boxes = []
        for element in page_layout:
            boxes.append(element.bbox)  # x1, y1, x2, y2
        boundingBoxes.append(boxes)
    return boundingBoxes


def convertPDFToImages(document, dpi=72):
    images = []
    images.extend(
        list(
            map(
                lambda image: cv2.cvtColor(np.array(image), code=cv2.COLOR_RGB2BGR),
                pdf2image.convert_from_path(document, dpi=dpi),
            )
        )
    )
    return images


def convertDPI(dpi, target, current, height):
    ratio = target / current
    return (int(dpi[0] * ratio), height - int(dpi[1] * ratio))


if __name__ == "__main__":
    render(sys.argv[1])
    pass
